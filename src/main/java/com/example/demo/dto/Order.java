package com.example.demo.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class Order {

    @NotBlank(message = "seller is empty")
    @Length(min = 9, max = 9, message = "seller length must be 9 digits")
    public String seller;

    @NotBlank(message = "customer is empty")
    @Length(min = 9, max = 9, message = "customer length must be 9 digits")
    public String customer;

    @Valid
    public List<Product> products;
}
