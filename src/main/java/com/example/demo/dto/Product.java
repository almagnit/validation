package com.example.demo.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
public class Product {

    @NotBlank(message = "product name is empty")
    public String name;
    @NotBlank(message = "product code is empty")
    @Length(min = 13, max = 13, message = "product code length must be 13 digits")
    public String code;

}
